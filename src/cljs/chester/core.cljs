(ns chester.core
  (:require [reagent.core :as reagent :refer [atom]]))

(enable-console-print!)

;; some constants
(def width              800)
(def height             400)
(def bolle-min-radius     3)
(def bolle-max-radius    10)
(def min-amount-boller  100)
(def max-amount-boller 3000)
(def button-height       65)

;; some random value generators
(defn randomizer [& vals]
  (fn []
    (rand-nth vals)))

(def rand-title
  (randomizer "Lad computeren tegne nogle boller!"
              "Ja, du klikkede og du fik boller."
              "Flere mønstre for dem som klikker."
              "Kan du tælle hvor mange der er?"
              "Du bliver ved med at kigge på boller."
              "Stipper på rød baggrund."
              "Der er vist noget overlap her."))

(def rand-shout
  (randomizer "Puhaa" "Whew" "Tadaa" "Boller Done"))

(def rand-color
  (randomizer "green" "yellow" "blue" "black" "brown" "orange"))

(defn rand-bolle
  []
  [:circle {:r (+ bolle-min-radius
                  (rand-int (- bolle-max-radius bolle-min-radius)))
            :cx (rand-int width)
            :cy (rand-int height)
            :fill (rand-color)}])

(defn rand-boller
  []
  (doall (for [x (range (+ min-amount-boller
                           (rand-int (- max-amount-boller min-amount-boller))))]
           ^{:key x} (rand-bolle))))

(defn rand-app-state
  []
  {:title (rand-title)
   :boller (rand-boller)
   :shout (rand-shout)})


;; the app-state in use will be the last value in the history atom
(defonce app-state-with-history (atom []))


;; the components
(defn bolle-tegner
  "Draws the canvas with the specified boller."
  [boller]
  [:div
   [:svg {:width width
          :height height
          :id "canvas"
          :style {:outline "2px solid black"
                  :background-color "red"}}
    boller]])

(defn push-button
  "Creates a push button with action fn."
  [txt fn]
  [:div 
   [:button {:on-click fn
             :style {:width width
                     :height button-height
                     :font-weight "bold"
                     :font-size "large"
                     :margin-top "1em"}}
    txt]])

(defn amount-label
  [amount shout]
  [:div
   {:style {:text-align "center"
            :width width
            :color "#666"
            :font-weight "bold"
            :margin-top "1em"}}
   (if amount
     (str "Har tegnet " amount " boller. " shout "!")
     "Jeg har jo ikke tegnet nogen boller. Jeg ved jo ikke hvordan man gør!")])

(defn app-component []
  (let [{:keys [title boller shout]} (last @app-state-with-history)]
    [:div
     [:h3 {:style {:text-align "center"}} (or title "Der mangler noget app-state.")]
     [bolle-tegner boller]
     [push-button "Klik nu på Mig!" (fn [_] (swap! app-state-with-history conj (rand-app-state)))]
     [amount-label (if boller (count boller)) shout]
     (if title
       [push-button "Undo" (fn [_] (swap! app-state-with-history pop))])]))

(reagent/render-component [app-component]
                          (. js/document (getElementById "app")))
