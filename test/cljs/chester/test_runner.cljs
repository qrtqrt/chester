(ns chester.test-runner
  (:require
   [doo.runner :refer-macros [doo-tests]]
   [chester.core-test]
   [chester.common-test]))

(enable-console-print!)

(doo-tests 'chester.core-test
           'chester.common-test)
